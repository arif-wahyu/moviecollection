//
//  MovieCollectionApp.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 25/02/21.
//

import SwiftUI

@main
struct MovieCollectionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
