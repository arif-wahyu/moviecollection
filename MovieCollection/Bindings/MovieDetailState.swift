//
//  MovieDetailState.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 27/02/21.
//

import SwiftUI

class MovieDetailState: ObservableObject {
    
    private let movieService: MovieService
    @Published var movie: Movie?
    @Published var isLoading = false
    @Published var error: NSError?
    @Published var reviews: [MovieReview]?
    @Published var totalPageReview: Int = 0
    
    init(movieService: MovieService = MovieStore.shared) {
        self.movieService = movieService
    }
    
    func loadMovie(id: Int) {
        self.movie = nil
        self.isLoading = false
        self.movieService.fetchMovie(id: id) {[weak self] (result) in
            guard let self = self else { return }
            
            self.isLoading = false
            switch result {
            case .success(let movie):
                self.movie = movie
            case .failure(let error):
                self.error = error as NSError
            }
        }
    }
    
    func loadMovieReview(id: Int, page: Int, completion: @escaping (Bool) -> ()) {
        self.movie = nil
        self.isLoading = false
        self.movieService.fetchMovieReview(id: id, page: page) {[weak self] (result) in
            guard let self = self else { return }
            self.isLoading = false
            switch result {
            case .success(let reviews):
                self.reviews = reviews.results
                self.totalPageReview = reviews.totalPages
                completion(false)
            case .failure(let error):
                self.error = error as NSError
                print(error)
                completion(true)
            }
        }
    }
}
