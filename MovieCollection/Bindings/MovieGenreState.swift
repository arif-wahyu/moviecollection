//
//  MovieGenreState.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 28/02/21.
//

import SwiftUI

class MovieGenreState: ObservableObject {
    @Published var genres: [MovieGenre]?
    @Published var isLoading: Bool = false
    @Published var error: NSError?

    private let movieService: MovieService
    
    init(movieService: MovieService = MovieStore.shared) {
        self.movieService = movieService
    }
    
    func loadGenre() {
        self.isLoading = true
        self.movieService.fetchGenreMovie() { [weak self] (result) in
            guard let self = self else { return }
            self.isLoading = false
            switch result {
            case .success(let response):
                self.genres = response.genres
                
            case .failure(let error):
                self.error = error as NSError
            }
        }
    }
    
}
