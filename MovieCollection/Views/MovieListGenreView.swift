//
//  MovieGenreView.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 28/02/21.
//

import SwiftUI

struct MovieListGenreView: View {
    
    @ObservedObject var movieGenreState = MovieGenreState()
    private let title: String = "Genres"
    
    var body: some View {
        NavigationView {
            List {
                LoadingView(isLoading: self.movieGenreState.isLoading, error: self.movieGenreState.error) {
                }
                
                if self.movieGenreState.genres != nil {
                    ForEach(self.movieGenreState.genres!) { genre in
                        NavigationLink(destination: MovieListByGenreView(genre: genre)) {
                            Text(genre.name)
                        }

                        //                        NavigationLink(destination: MovieDetailView(movieId: movie.id)) {
                        //                            VStack(alignment: .leading) {
                        //                                Text(movie.title)
                        //                                Text(movie.yearText)
                        //                            }
                        //                        }
                    }
                }
                
            }
            .onAppear {
                self.movieGenreState.loadGenre()
            }
            .navigationBarTitle(title)
        }
    }
}

struct MovieListGenreView_Previews: PreviewProvider {
    static var previews: some View {
        MovieListGenreView()
    }
}
