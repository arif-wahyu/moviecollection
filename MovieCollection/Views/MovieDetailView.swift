//
//  MovieDetailView.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 27/02/21.
//

import SwiftUI

struct MovieDetailView: View {
    
    let movieId: Int
    @ObservedObject private var movieDetailState = MovieDetailState()
    @State var reviews: [MovieReview] = []
    @State var page: Int = 1
    
    var body: some View {
        ZStack {
            LoadingView(isLoading: self.movieDetailState.isLoading, error: self.movieDetailState.error) {
                self.movieDetailState.loadMovie(id: self.movieId)
            }
            
            if movieDetailState.movie != nil {
                MovieDetailListView(movie: self.movieDetailState.movie!, reviews: $reviews, movieDetailState: movieDetailState, page: $page)
                
            }
        }
        .navigationBarTitle(movieDetailState.movie?.title ?? "")
        .onAppear {
            self.movieDetailState.loadMovie(id: self.movieId)
            self.movieDetailState.loadMovieReview(id: self.movieId, page: page) { (error) in
                if !error {
                    for review in                     self.movieDetailState.reviews! {
                        print("ksldaksdlaksnd")
                        self.reviews.append(review)
                    }
                    
                } else {
                    print("bener banger ini")
                }
            }
        }
    }
}

struct MovieDetailListView: View {
    
    let movie: Movie
    @Binding var reviews: [MovieReview]
    @ObservedObject var movieDetailState: MovieDetailState
    @Binding var page: Int
    @State private var selectedTrailer: MovieVideo?
    let offset: Int = 4
    let imageLoader = ImageLoader()
    
    var body: some View {
        List {
            MovieDetailImage(imageLoader: imageLoader, imageURL: self.movie.backdropURL)
                .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
            
            HStack {
                Text(movie.genreText)
                Text("·")
                Text(movie.yearText)
                Text(movie.durationText)
            }
            
            Text(movie.overview)
            HStack {
                if !movie.ratingText.isEmpty {
                    Text(movie.ratingText).foregroundColor(.yellow)
                }
                Text(movie.scoreText)
            }
            
            Divider()
            
            HStack(alignment: .top, spacing: 4) {
                if movie.cast != nil && movie.cast!.count > 0 {
                    VStack(alignment: .leading, spacing: 4) {
                        Text("Starring").font(.headline)
                        ForEach(self.movie.cast!.prefix(9)) { cast in
                            Text(cast.name)
                        }
                    }
                    .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                    Spacer()
                    
                }
                
                if movie.crew != nil && movie.crew!.count > 0 {
                    VStack(alignment: .leading, spacing: 4) {
                        if movie.directors != nil && movie.directors!.count > 0 {
                            Text("Director(s)").font(.headline)
                            ForEach(self.movie.directors!.prefix(2)) { crew in
                                Text(crew.name)
                            }
                        }
                        
                        if movie.producers != nil && movie.producers!.count > 0 {
                            Text("Producer(s)").font(.headline)
                                .padding(.top)
                            ForEach(self.movie.producers!.prefix(2)) { crew in
                                Text(crew.name)
                            }
                        }
                        
                        if movie.screenWriters != nil && movie.screenWriters!.count > 0 {
                            Text("Screenwriter(s)").font(.headline)
                                .padding(.top)
                            ForEach(self.movie.screenWriters!.prefix(2)) { crew in
                                Text(crew.name)
                            }
                        }
                    }
                    .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                }
            }
            
            Divider()
            
            if movie.youtubeTrailers != nil && movie.youtubeTrailers!.count > 0 {
                Text("Trailers").font(.headline)
                
                ForEach(movie.youtubeTrailers!) { trailer in
                    Button(action: {
                        self.selectedTrailer = trailer
                    }) {
                        HStack {
                            Text(trailer.name)
                            Spacer()
                            Image(systemName: "play.circle.fill")
                                .foregroundColor(Color(UIColor.systemBlue))
                        }
                    }
                }
            }
            
            Divider()
            
            if !reviews.isEmpty {
                Text("Reviews").font(.headline)
                ForEach(reviews, id: \.self) { review in
                    MovieDetailReview(review: review)
                        .onAppear {
                            if page > movieDetailState.totalPageReview {
                                self.listItemAppears(review)
                            }
                        }
                }
            } else {
                Text("Reviews").font(.headline)
                Text("No review available").font(.subheadline)
            }
        }
        .sheet(item: self.$selectedTrailer) { trailer in
            SafariView(url: trailer.youtubeURL!)
        }
    }
    
}

struct MovieDetailReview: View {
    /* Indicates whether the user want to see all the text or not. */
    let review: MovieReview
    @State private var expanded: Bool = false
    
    /* Indicates whether the text has been truncated in its display. */
    @State private var truncated: Bool = false
    
    var body: some View {
        VStack (alignment: .leading){
            Text(review.author).font(.headline)
                .padding(.bottom, 4)
            Text(review.content).font(.subheadline)
                .lineLimit(self.expanded ? nil : 3)
                .background(GeometryReader { geometry in
                    Color.clear.onAppear {
                        self.determineTruncation(geometry, text: review.content)
                    }
                })
            if self.truncated {
                self.toggleButton
            }
        }.padding(.vertical)
    }
    
    var toggleButton: some View {
        Button(action: { self.expanded.toggle() }) {
            Text(self.expanded ? "Show less" : "Show more")
                .font(.caption)
                .foregroundColor(Color.blue)
        }.padding(.top)
    }
    
    private func determineTruncation(_ geometry: GeometryProxy, text: String) {
        // Calculate the bounding box we'd need to render the
        // text given the width from the GeometryReader.
        let total = text.boundingRect(
            with: CGSize(
                width: geometry.size.width,
                height: .greatestFiniteMagnitude
            ),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.systemFont(ofSize: 16)],
            context: nil
        )
        
        if total.size.height > geometry.size.height {
            self.truncated = true
        }
    }
}

struct MovieDetailImage: View {
    
    @ObservedObject var imageLoader: ImageLoader
    let imageURL: URL
    
    var body: some View {
        ZStack {
            Rectangle().fill(Color.gray.opacity(0.3))
            if self.imageLoader.image != nil {
                Image(uiImage: self.imageLoader.image!)
                    .resizable()
            }
        }
        .aspectRatio(16/9, contentMode: .fit)
        .onAppear {
            self.imageLoader.loadImage(with: self.imageURL)
        }
    }
}

struct MovieDetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MovieDetailView(movieId: Movie.stubbedMovie.id)
        }
    }
}
