//
//  MovieListByGenreView.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 28/02/21.
//

import SwiftUI

struct MovieListByGenreView: View {
    let genre: MovieGenre
    @State var movies: [Movie] = []
    @ObservedObject var movieListState = MovieListState()
    let offset: Int = 4
    @State var page: Int = 1
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    
    var body: some View {
        ScrollView {
            Group {
                if !self.movies.isEmpty {
                    LazyVGrid(columns: columns, spacing: 20) {
                        ForEach(self.movies) { movie in
                            NavigationLink(destination: MovieDetailView(movieId: movie.id)) {
                                MoviePosterCard(frameWidth: 144, frameHeight: 246, movie: movie)
                            }
                            .onAppear {
                                self.listItemAppears(movie)
                            }
                        }
                    }
                    .padding(.horizontal)
                    
                } else {
                    LoadingView(isLoading: self.movieListState.isLoading, error: self.movieListState.error) {
                        self.movieListState.loadMoviesByGenre(id: genre.id, page: page) { (error) in
                            if !error {
                                for movie in self.movieListState.movies! {
                                    self.movies.append(movie)
                                }
                            }
                        }
                    }
                }
            }
            .navigationBarTitle(genre.name)
            .onAppear {
                self.movieListState.loadMoviesByGenre(id: genre.id, page: page) { (error) in
                    if !error {
                        for movie in self.movieListState.movies! {
                            self.movies.append(movie)
                        }
                    }
                }
            }
        }
        
    }
}

struct MovieListByGenreView_Previews: PreviewProvider {
    static var previews: some View {
        MovieListByGenreView(genre: Movie.stubbedGenre)
    }
}
