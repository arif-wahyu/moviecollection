//
//  ContentView.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 25/02/21.
//
import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            MovieListView()
                .tabItem {
                    VStack {
                        Image(systemName: "tv")
                        Text("Movies")
                    }
            }
            .tag(0)
            
            MovieListGenreView()
                .tabItem {
                    VStack {
                        Image(systemName: "book")
                        Text("Genres")
                    }
            }
            .tag(1)
            
            MovieSearchView()
                .tabItem {
                    VStack {
                        Image(systemName: "magnifyingglass")
                        Text("Search")
                    }
            }
            .tag(2)
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
