//
//  Movie+Stub.swift
//  MovieCollection
//
//  Created by Arif Wahyu on 25/02/21.
//

import Foundation

extension Movie {
    
    static var stubbedMovies: [Movie] {
        let response: MovieResponse? = try? Bundle.main.loadAndDecodeJSON(filename: "movie_list")
        return response!.results
    }
    
    static var stubbedMovie: Movie {
        stubbedMovies[0]
    }
    
    static var stubbedGenre: MovieGenre {
        let genre: MovieGenre? = nil
        return genre!
    }
    
}

extension Bundle {
    
    func loadAndDecodeJSON<D: Decodable>(filename: String) throws -> D? {
        guard let url = self.url(forResource: filename, withExtension: "json") else {
            return nil
        }
        let data = try Data(contentsOf: url)
        let jsonDecoder = Utils.jsonDecoder
        let decodedModel = try jsonDecoder.decode(D.self, from: data)
        return decodedModel
    }
}

extension RandomAccessCollection where Self.Element: Identifiable {
    func isThresholdItem<Item: Identifiable>(offset: Int,
                                             item: Item) -> Bool {
        guard !isEmpty else {
            return false
        }
        
        guard let itemIndex = firstIndex(where: { AnyHashable($0.id) == AnyHashable(item.id) }) else {
            return false
        }
        
        let distance = self.distance(from: itemIndex, to: endIndex)
        let offset = offset < count ? offset : count - 1
        return offset == (distance - 1)
    }
}

extension MovieListByGenreView {
    func listItemAppears<Item: Identifiable>(_ item: Item) {
        if movieListState.movies != nil {
            if movieListState.movies!.isThresholdItem(offset: offset,
                                                      item: item) {
                self.page += 1
                self.movieListState.loadMoviesByGenre(id: genre.id, page: page) { (error) in
                    if !error {
                        for movie in self.movieListState.movies! {
                            self.movies.append(movie)
                        }
                    }
                }
            }
        }
    }
}

extension MovieDetailListView {
    func listItemAppears<Item: Identifiable>(_ item: Item) {
        if movieDetailState.reviews != nil {
            if movieDetailState.reviews!.isThresholdItem(offset: offset,
                                                      item: item) {
                self.page += 1
                self.movieDetailState.loadMovieReview(id: movie.id, page: page) { (error) in
                    if !error {
                        for review in self.movieDetailState.reviews! {
                            self.reviews.append(review)
                        }
                    }
                }
            }
        }
    }
}
